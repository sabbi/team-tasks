const fs = require("fs");
const path = require("path");
const { GitLabAPI, GitLabAPIIterator } = require("gitlab-api-async-iterator")();
const { DateTime } = require("luxon");

const now = DateTime.local().toUTC();
const currentProject = "14402567";

async function getNextMilestone() {
  const nextMilestone = now.plus({ months: 1 });
  console.log(`Next month will be ${nextMilestone.toString()}`);
  const milestones = new GitLabAPIIterator(
    `/projects/${currentProject}/milestones`,
    { include_parent_milestones: true, state: "active" }
  );
  for await (const milestone of milestones) {
    const { start_date, due_date, title } = milestone;
    const start = DateTime.fromISO(start_date);
    const due = DateTime.fromISO(due_date);
    console.log(
      `Checking: ${title} which is from ${start_date} to ${due_date}`
    );
    if (
      /^\d\d.\d+$/.test(title) &&
      start < nextMilestone &&
      nextMilestone <= due
    ) {
      return milestone;
    }
  }
}

async function createPlanningIssueIfNecessary() {
  const nextMilestone = await getNextMilestone();
  if (!nextMilestone) {
    console.warn("Could not determine next milestone");
    return;
  }

  console.log(
    `Next Milestone is ${nextMilestone.title}, Let's check whether there is an issue for it`
  );

  const { data: issues } = await GitLabAPI(
    `/projects/${currentProject}/issues`,
    { params: { milestone: nextMilestone.title, labels: "Planning Issue" } }
  );

  if (issues.length) {
    console.log(
      `Seems like ${nextMilestone.title} has a planning issue already: ${issues[0].web_url}`
    );
    return;
  }

  console.log(`Trying to create the issue for ${nextMilestone.title}`);

  try {
    const description = fs
      .readFileSync(
        path.join(__dirname, ".gitlab", "issue_templates", "planning_issue.md"),
        "utf8"
      )
      .replace(/00\.0/g, nextMilestone.title);

    const payload = {
      title: `Ecosystem ${nextMilestone.title} Planning issue`,
      milestone_id: nextMilestone.id,
      due_date: nextMilestone.start_date,
      labels: "Planning Issue",
      description,
    };

    const { data: created } = await GitLabAPI.post(
      `/projects/${currentProject}/issues`,
      payload
    );
    console.log(`Successfully created: ${created.web_url}`);
  } catch (e) {
    console.log(`Could not create issue for ${nextMilestone.title}`);
    console.log(e);
  }
}

async function main() {
  await createPlanningIssueIfNecessary();
}

main().catch((e) => {
  console.log("ERROR", e);
  process.exit(1);
});
