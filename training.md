# Training Resources

This is a compilation of useful training resources.


## On-call

- [Tips & Tricks of Troubleshooting](https://about.gitlab.com/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html#tips--tricks-of-troubleshooting)
- [Tools for Engineers](https://about.gitlab.com/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html#tools-for-engineers)


## Memory, Performance & Profiling

- [RoR Application Performance Training](https://gitlab.com/gitlab-org/memory-team/team-tasks/issues/35)

From the [memory team onboarding](https://gitlab.com/gitlab-org/memory-team/memory-team-onboarding/blob/master/.gitlab/issue_templates/Memory-team_onboarding.md) and [Memory 101 & 102](https://gitlab.com/gitlab-org/memory-team/memory-team-onboarding/issues/1):

- [Performance Guidelines](https://docs.gitlab.com/ce/development/performance.html)
- [Memory Team 101](https://youtu.be/enZ9zptATeY)
- [Memory Team 102](https://youtu.be/idQXtzC6QV8)
- [Prometheus 101](https://youtu.be/8Ai55-sYJA0)


## Gitaly

- [GitLab Developers Guide to Working with Gitaly](https://docs.gitlab.com/ee/development/gitaly.html)
- [Beginner's guide to Gitaly contributions](https://gitlab.com/gitlab-org/gitaly/blob/master/doc/beginners_guide.md)
- [Gitaly presentations](https://gitlab.com/gitlab-org/gitaly#presentations)


## Security

- [GitLab Security Secure Coding Training](https://about.gitlab.com/handbook/engineering/security/secure-coding-training.html)


## Deep Dives

- [Create Deep Dives](https://gitlab.com/gitlab-org/create-stage/issues/1)
- [CI/CD Deep Dive](https://about.gitlab.com/handbook/marketing/product-marketing/demo/cicd-deep/)
- [GitLab Unfiltered YouTube](https://www.youtube.com/playlist?list=PL05JrBw4t0KqsiWzD0YZYp9xSxgTRoWxH)
- [Any relevant page within GitLab Docs](https://docs.gitlab.com/search/?q=deep+dives)


## Authentication Deep Dive

- [Session 1 Recording](https://drive.google.com/file/d/16hDb4lHXril_1UmchI5_NlH8iN_b6Kdl/view?usp=sharing): Background and setting up authentication in GitLab
- [Session 2 Recording](https://drive.google.com/file/d/1nNFX-v1AvCaoibDrcw57jxD59BR-sT8Z/view?usp=sharing): Troubleshooting authentication issues
- [Slides for the deep dive](https://docs.google.com/presentation/d/1S8IrmKBLMOsSxEJNQHBLja1ax3qZ0YFicUBM_RFtvVg/edit?usp=sharing)


## GraphQL

- [GraphQL Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpcjeHjaRMB7IGB2oDWyJzv)


## Distribution

- [Distribution Team Demos playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrPasGZcEUoHHIYdUtzpfA4)


## Support Bootcamps

- [List of bootcamps](https://about.gitlab.com/handbook/support/advanced-topics/)


## UX

- [UX Onboarding](https://gitlab.com/gitlab-org/gitlab-design/-/blob/master/.gitlab/issue_templates/UX%20Onboarding.md) with [Tech knowledge to know](https://gitlab.com/gitlab-org/gitlab-design/-/blob/master/.gitlab/issue_templates/UX%20Onboarding.md#tech-knowledge-to-know-computer)

