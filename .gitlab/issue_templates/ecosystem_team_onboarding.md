## [First Name], Welcome to GitLab and to the Ecosystem Team!

We are We are all excited that you are joining us on the [Ecosystem Team](https://about.gitlab.com/handbook/engineering/development/enablement/ecosystem/).  You should have already received an onboarding issue from [The People Group](https://about.gitlab.com/handbook/people-group/) familiarizing yourself with GitLab, setting up accounts, accessing tools, etc.  This onboarding issue is specific to the Ecosystem Team people, processes and setup.

For the first week or so you should focus on your GitLab onboarding issue.  There is a lot of information there, and the first week is very important to get your account information set up correctly.  Tasks from this issue can start as you get ready to start contributing to the Ecosystem team.

Much like your GitLab onboarding issue, each item is broken out by Owner: Action.  Just focus on "New Team Member" items, and feel free to reach out to your team if you have any questions.

### First Day

* [ ] Manager: Invite team member to #g_ecosystem Slack Channel
* [ ] Manager: Invite team member to weekly Ecosystem Team Meeting
* [ ] Manager: Add new team member to [Ecosystem Team Retro](https://gitlab.com/gl-retrospectives/ecosystem/-/project_members)

### First Week
* [ ] Manager: Add new team member to [Geekbot standup](https://geekbot.com/dashboard/)
* [ ] Manager: Add new team member introduction to the [Engineering Week In Review](https://docs.google.com/document/d/1Oglq0-rLbPFRNbqCDfHT0-Y3NkVEiHj6UukfYijHyUs/edit)
* [ ] Manager: Add new team member to [Periscope Dashboard](https://app.periscopedata.com/app/gitlab/533477/WIP:-Ecosystem-Development-Metrics). [Instructions](https://about.gitlab.com/handbook/engineering/management/#custom-team-metrics-dashboard)
* [ ] New team member: Read about your team, its mission, team members and resources on the [Ecosystem Team page](https://about.gitlab.com/handbook/engineering/development/enablement/ecosystem/)
* [ ] New team member: Set up [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) to meet your team

### Second Week
* [ ] New team member: Read about how GitLab uses [labels](https://docs.gitlab.com/ee/user/project/labels.html)
* [ ] New team member: Familiarize yourself with the team boards
  * [ ] [Ecosystem Planning](https://gitlab.com/groups/gitlab-org/-/boards/1167634) is our roadmap. It helps us plan which issues we want to work on for the next releases.
  * [ ] [Ecosystem Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1290820) enables us to see the state of each issue.
  * [ ] [Ecosystem by Team Member](https://gitlab.com/groups/gitlab-org/-/boards/1365136) visualizes issues assigned to each team member.
    * [ ] Add yourself to the [team members board](https://gitlab.com/groups/gitlab-org/-/boards/1365136)
* [ ] New team member: Read about the Ecosystem stage strategy and product category visions
  * [ ] [Ecosystem Strategy](https://about.gitlab.com/direction/ecosystem/)
  * [ ] [Integrations](https://about.gitlab.com/direction/ecosystem/integrations/)
* [ ] New team member: Ensure that you are able to access our integration lab environments. Ask a team member to help you gain access if needed.
  * [ ] JIRA self-hosted
    * http://jira.reali.sh:8080
    * Create your own account. Ask team member to grant admin access if required.
  * [ ] JIRA cloud
    * https://olearycrew.atlassian.net

