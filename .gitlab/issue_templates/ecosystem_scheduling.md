Please take a look at the [enter milestone here] column on the [planning board](https://gitlab.com/groups/gitlab-org/-/boards/1167634) or related issues linked below and let us know if there are any questions or comments about the scheduled items.

Note that we still want to keep any discussion about a specific issue in the issue itself. The purpose of this issue is to discuss workload and priority as well as to suggest other issues to bring into the milestone or move from the milestone.

According to [the product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline), we should have the release scope finalized by the 13th of the month.
